struct auto {
  char modelo[10];
  char marca[9];
  float precio;
  float peso;
  int noPuertas;
  float velocida;
};

struct computadoras {
  int ram;
  int rom;
  char procesador[50];
  char marca[20];
  char modelo[30];
  float tamanio;
  char tajetaGrafica[50];
  char tieneTarjetaGrafica[3];

};
struct alumno{
  char nombre[30];
  int edad;
  float altura;
  float peso;
  char genero;
};
struct equipoDeFutbol {
  char alineacion[10];
  char nombre[30];
};
struct mascota {
  char raza[20];
  float estatura;
  char color[10];
  float peso;
  char nombre[30];
};
struct videoJuego {
  char titulo[30];
  char desarrolladora[20];
  char plataforma[27];
};
struct superHeroe {
  char nombre[15];
  char poderes[30];
  char debilidad[100];
  char historia[200];
};
struct planetas {
  int nombreCientifico;
  char nombre[20];
  float dimenciones;
  int satelites;
  float distancia;

};
struct perdonajeDeTuJuegoFavorito {
  char nombJuego[20];
  char nombDelPeronaje[17];
  char genero;
  char avilidades[50];
  char vestimenta;
  char colorDePiel[10];
  char especie[20];
};
struct comic {
  char nombre[20];
  char editorial[15];
  int numDeComic;
  int numPaginas;
  float dimenciones;
};
