#include <stdio.h>
int main(int argc, char const *argv[]) {
   printf("El valor en bytes de int es: %lu\n",sizeof(int));
   printf("El valor en bytes de float es: %lu\n",sizeof(float));
   printf("El valor en bytes de char es: %lu\n",sizeof(char));
   printf("El valor en bytes de long int es: %lu\n",sizeof(long int));
  return 0;
}
